#pragma once

/**
 * DateTime
 * -----------------
 * FileName     : DateTime.h
 * Author       : Sahil Gupta
 * Date         : 7 February 2017
 * Version      : 1.0
 *
 * Public Interface
 * -------------
 * static std::string GetCurrentDateTime()
 * static bool IsOlderThan(const std::string& Reference, const std::string& Value)
 *
 * Required files
 * -------------
 * None
 *
 * Maintenance History
 * -------------------
 * ver 1.0 : 30 January 2017
 *  - first release
 */

#include <string>
#include <sstream>
#include <iomanip>

class DateTimeClass
{
public:
    static std::string GetCurrentDateTime();
    static bool IsOlderThan(const std::string& Reference, const std::string& Value);
};

std::string DateTimeClass::GetCurrentDateTime()
{
    //char ctime[32];
    //struct tm newtime;

    //time_t now = time(0);
    //localtime_s(&newtime, &now);
    //asctime_s(ctime, 32, &newtime);
    //std::string DT(ctime);
    //return DT.substr(0, DT.size() - 1);

    time_t t = time(nullptr);
    tm tm;
    localtime_s(&tm, &t);
    std::stringstream ss;
    ss << std::put_time(&tm, "%Y/%d/%m %H:%M:%S");
    return ss.str();
}

/* Enter date in "YYYY/DD/MM HH:MM:SS" in 24-hour format
 * Return true if date specified by Value comes after reference date
 */
bool DateTimeClass::IsOlderThan(const std::string& Reference, const std::string& Value)
{
    if (Reference.compare(Value) >= 0)
        return false;

    return true;
}