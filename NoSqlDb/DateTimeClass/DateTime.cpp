/**
 * DateTime Test
 * ---------------------
 * Tests functionality of DateTime Class
 *
 * FileName     : DateTime.cpp
 * Author       : Sahil Gupta
 * Date         : 7 February 2017
 * Version      : 1.0
 */

#include <iostream>
#include <string>
#include <ctime>

#include "DateTime.h"

int main()
{
    std::cout << DateTimeClass::GetCurrentDateTime() << std::endl;

    std::string D1 = "2017/08/02 00:18:21";
    std::string D2 = "2017/07/02 00:18:21";
    std::cout << DateTimeClass::IsOlderThan(D1,D2) << std::endl;
}